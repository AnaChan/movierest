#SETUP
* if you dont have php and mysql installed, install xampp, this project was created using xampp-windows-x64-7.2.31-0-VC15-installer
* clone the project
* run xampp control
* Start MySql
* create database with name movie_rental
* config data base connection on  config/database.php
	- under mysql connection add: DB_HOST, DB_DATABASE, DB_USERNAME, DB_PASSWORD

* (gitbash) go to the project's folder 
* run this command:
```composer install```  


##Database creation
* (gitbash) go to the project's folder 
* generate the migration
* go to your project's folder


    php artisan migrate
* reset database in case you need to drop all tables
```php artisan migrate:reset``` 
	
* generate the seeds


```composer dump-autoload```


```php artisan db:seed```


#RUN
* (gitbash) go to the project's folder run:
```php artisan serve```

#Test the app
* import postman collection with the name RENTAL_MOVIE
* register admin user
* register client user
* login (to test the apis you need to login and then copy the token to the token param)
* save movie
* getAllMovies
* get single movie
* update movie
* remove movie (change availability)
* delete movie
* rent a movie
* purchase a movie
* get all rentals
* get all purchases
* like (like a movie)

