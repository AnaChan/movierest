<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMoviesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       if (!Schema::hasTable('movies')) {
            Schema::create('movies', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->string('title', 200);
                $table->string('description',1000);
                $table->integer('stock');
                $table->float('rental_price',5,2);
                $table->float('sale_price',5,2);
                $table->float('penalty_by_day',5,2);
                $table->boolean('availability'); //admin only
                $table->string('img_url', 1000);
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('movies');
    }
}
