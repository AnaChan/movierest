<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePurchasesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('purchases')) {
            Schema::create('purchases', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->float('total_amount',5,2);
                $table->integer('qty');
                $table->timestamps();
                //FK
                $table->unsignedBigInteger('client_id');
                $table->unsignedBigInteger('movie_id');
                $table->foreign('client_id')->references('id')->on('users')->onDelete('cascade');
                $table->foreign('movie_id')->references('id')->on('movies')->onDelete('cascade');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('purchases');
    }
}
