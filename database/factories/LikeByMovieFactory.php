<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\LikeByMovie;
use Faker\Generator as Faker;

$factory->define(LikeByMovie::class, function (Faker $faker) {
    return [
        'user_id' =>  rand(2,3),
        'movie_id' => rand(1,10)
    ];
});
