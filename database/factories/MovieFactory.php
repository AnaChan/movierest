<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Movie;
use Faker\Generator as Faker;

$factory->define(Movie::class, function (Faker $faker) {
    return [
        'title' => "naruto " . rand(1,5),
        'description' => Str::random(50),
        'stock' => 3,
        'rental_price' => 10.00,
        'sale_price' => 20.00,
        'penalty_by_day' => 3,
        'availability' => rand(0,1),
        'img_url' => "https://i.ytimg.com/vi/IZH5Ijtym9Q/maxresdefault.jpg",
    ];
});
