<?php

use Illuminate\Database\Seeder;
use App\Rental;
use Carbon\Carbon;

class RentalSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('rentals')->insert([
            'start_date' => now(),
            'end_date' => Carbon::now()->addDays(3),
            'total_amount' => 10.00,
            'qty' =>  2,
            'client_id' =>  2,
            'movie_id' =>  4,
            'created_at' => Carbon::now()
        ]);

        DB::table('rentals')->insert([
            'start_date' => now(),
            'end_date' =>  Carbon::now()->addDays(3),
            'total_amount' => 15.00,
            'qty' =>  3,
            'client_id' =>  2,
            'movie_id' =>  6,
            'created_at' => Carbon::now()
        ]);

        DB::table('rentals')->insert([
            'start_date' => now(),
            'end_date' =>  Carbon::now()->addDays(5),
            'total_amount' => 10.00,
            'qty' =>  2,
            'client_id' =>  3,
            'movie_id' =>  2,
            'penalty_amount' => 0
        ]);
    }
}
