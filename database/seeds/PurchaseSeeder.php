<?php

use Illuminate\Database\Seeder;
use App\Purchase;
use Carbon\Carbon;

class PurchaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('purchases')->insert([
            'total_amount' => 30.00,
            'qty' => 1,
            'client_id' => 2,
            'movie_id' => 1,
            'created_at' => Carbon::now()
        ]);

        DB::table('purchases')->insert([
            'total_amount' => 30.00,
            'qty' => 1,
            'client_id' => 3,
            'movie_id' => 2,
            'created_at' => Carbon::now()
        ]);
    }
}
