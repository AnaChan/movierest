<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UserSeeder::class);
        $this->call(RoleSeeder::class);
        $this->call(UserSeeder::class);
        $this->call(MovieSeeder::class);
        $this->call(PurchaseSeeder::class);
        $this->call(RentalSeeder::class);
        $this->call(LikeByMovieSeeder::class);
    }
}
