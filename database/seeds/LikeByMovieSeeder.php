<?php

use Illuminate\Database\Seeder;
use App\LikeByMovie;

class LikeByMovieSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $like = factory(LikeByMovie::class)->make();   
        factory(LikeByMovie::class, 20)->create();
        
    }
}
