<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class MovieLog extends Model
{
    protected $fillable = ['field', 'value', 'updated_by'];

    public function saveLog( $field, $oldVlue, $newValue )
    {
        $user = Auth::user();
        DB::table('movie_logs')->insert(
                                ['field' => $field,
                                 'from_value' => $oldVlue,
                                 'to_value' => $newValue,
                                 'updated_by' => $user->id,
                                 'created_at' => Carbon::now()
                                ]
                            );

   }
}
