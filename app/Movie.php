<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Movie extends Model
{
    protected $fillable = [
        'title', 'description', 'stock', 'rental_price', 'sale_price','penalty_by_day', 'availability', 'img_url'
    ];

     protected $casts = [
        'availability' => 'boolean',
    ];
}
