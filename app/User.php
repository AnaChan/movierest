<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Tymon\JWTAuth\Contracts\JWTSubject;
use App\Movie;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','roleId'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'created_at' => 'timestamp',
        'updated_at' => 'timestamp',
    ];

    /**
    * @return token
    * @createdBy anaro87
    * @createdAt 6/13/2020
    * @Description: get jwt token key
    */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
    * @return token
    * @createdBy anaro87
    * @createdAt 6/13/2020
    */
    public function getJWTCustomClaims()
    {
        return [];
    }

    /**
    * @return token
    * @createdBy anaro87
    * @createdAt 6/13/2020
    */
    public function movies()
    {
        return $this->hasMany(Movie::class);
    }

    /**
    * @param user id
    * @return token
    * @createdBy anaro87
    * @createdAt 6/13/2020
    * @Description: get login user's role
    */
    public function getUserRole($id) :String 
    { 
        $roles = DB::table('roles')
            ->join('users', 'users.role_id', '=', 'roles.id')
            ->where('users.id', $id)
            ->select('roles.name')
            ->get();

        $roleName = $roles->map(function ($name) {
            return $name;
        });

        return $roles[0]->name;

    }

    /**
    * @param user id
    * @return token
    * @createdBy anaro87
    * @createdAt 6/13/2020
    * @Description: validate if user is admin
    */
    public function isAdmin()
    {
        $authUser = Auth::user();
        if(isset($authUser)){
            $userRole = $this->getUserRole($authUser->id);
        
            if ($userRole != "admin") {
                return false;
            }else{ return true;}
        }else { return false;}
    }

}//classs end
