<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\Rental;
use App\Movie;
use Carbon\Carbon;

class RentalController extends Controller
{
    /**
    * @return mixed
	* @createdBy anaro87
	* @createdAt 6/13/2020
	* @Description: GET all
	*/
    public function index()
    {
        $rentalObj = new Rental();
        $results = DB::table('rentals')->get();

        $data = [];

        foreach ($results as $result) {
        	$Obj = new Rental();
        	$movie = Movie::find($result->movie_id);
	        $user = User::find($result->client_id);
			$Obj->start_date = $result->start_date;
			$Obj->end_date = $result->end_date;
			if($result->returned == true){
			   		$Obj->penalty_amount = $result->penalty_amount;
	        }else{
	        		$Obj->penalty_amount = $rentalObj->calculatePenalty($result->end_date, $movie->penalty_by_day);
	        }
			  
			$Obj->returned = $result->returned;
			$Obj->qty = $result->qty;
			$Obj->total_amount = $result->total_amount;
			$Obj->movie = $movie->title;
			$Obj->user = $user->email;
			$data[] = $Obj;
        }
        
        return  $data;
            
    }

    /**
    * @param $id
 	* @return \Illuminate\Http\JsonResponse
	* @createdBy anaro87
	* @createdAt 6/13/2020
	* @Description: GET by id
	*/
    public function show($id)
    {
        $rental = Rental::find($id);
    
        if (!$rental) {
            return response()->json([
                'success' => false,
                'message' => 'Sorry, movie with id ' . $id . 'was not be found'
            ], 400);
        }
    
        return $rental;
    }

    /**
    * @param Request $request
	* @return \Illuminate\Http\JsonResponse
	* @throws \Illuminate\Validation\ValidationException
	* @createdBy anaro87
	* @createdAt 6/13/2020
	* @Description: POST rent a movie 
	*/
    public function store(Request $request)
    {
       
        try {
            
             // dd($request->all());
            $isValid = $request->validate([
                'start_date' => 'required',
                'end_date' => 'required',
                'qty' => 'required|integer',
                'total_amount' => 'required|regex:/^\d*(\.\d{2})?$/',
                'client_id' => 'required',
                'movie_id' => 'required'
            ]);

            $movie = Movie::find($request->movie_id);

	        if (!$movie) {
	            return response()->json([
	                'success' => false,
	                'message' => "Sorry, movie with id  {$request->movie_id} was not be found"
	            ], 400);
	        }

			//calculate total amount
            $totalAmount = $request->qty * $movie->sale_price;
           
            $rental = new Rental();
            $rental->start_date = $request->start_date;
            $rental->end_date = $request->end_date;
            $rental->qty = $request->qty;
            $rental->total_amount = $totalAmount;
            $rental->client_id = $request->client_id;
            $rental->movie_id = $request->movie_id;
            $rental->returned = false;
            $rental->created_at = now();
            $insert = $rental->save();

            if ($insert) {
                return response()->json([
                    'success' => true,
                ]);
            } else {
                return response()->json([
                    'success' => false,
                    'message' => "Sorry, You couldn't rent the movie"
                ], 500);
            }
            
        } catch (Exception $e) {
            var_dump($e);
            
        }
    }
    
}



