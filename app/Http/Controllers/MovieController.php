<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use JWTAuth;
use App\Movie;
use App\MovieLog;
use App\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;

class MovieController extends Controller
{
    protected $user;

    public function __construct()
    {
        //$this->user = JWTAuth::parseToken()->authenticate(); for check
    }

    /**
    * @return mixed
	* @createdBy anaro87
	* @createdAt 6/13/2020
	* @Description: GET all
	*/
    public function index(Request $request)
    {
        //validate admin role
        $user = New User();
        $isAdmin = $user->isAdmin() == true ? true : $user->isAdmin();
        $order = 'desc';
        if(isset($request->order)){
            $order = $request->order;
        }

        $movies = DB::table('movies')
            ->selectRaw('movies.*, count(like_by_movies.movie_id) AS `likes`')
            ->leftJoin('like_by_movies', 'movies.id', '=', 'like_by_movies.movie_id')
            ->groupBy('movies.id','movies.title', 'movies.description', 'movies.stock', 'movies.rental_price', 'movies.sale_price','movies.penalty_by_day', 'movies.availability','movies.created_at','movies.updated_at', 'movies.img_url');
        if(isset($request->availability) && isset($request->title) && $isAdmin == true){
            var_dump("TEST");
            $movies = $movies->where([ 
                    ['movies.availability', '=', $request->boolean('availability')]
                    ,['movies.title', 'LIKE', '%'. $request->title .'%']
                    ]);
        } elseif (isset($request->availability) && $isAdmin == true) {
            $movies = $movies->where( 'movies.availability', '=', $request->boolean('availability'));
        } elseif(isset($request->title)) {
            $movies = $movies->where('movies.title', 'LIKE', '%'. $request->title .'%');
        }

        $movies = $movies
            ->orderBy('likes',$order)
            ->orderBy('movies.title', $order);
            //->get();

        if(isset($request->per_page)){
            $movies = $movies->paginate($request->per_page);
        }else{
            $movies = $movies->paginate();
        }
        
        //return  $movies->toArray();
        return $movies;
            
    }

    /**
    * @param $id
 	* @return \Illuminate\Http\JsonResponse
	* @createdBy anaro87
	* @createdAt 6/13/2020
	* @Description: GET by id
	*/
    public function show($id)
    {
        $movie = Movie::find($id);
    
        if (!$movie) {
            return response()->json([
                'success' => false,
                'message' => "Sorry, movie with id {$id} was not be found"
            ], 400);
        }
    
        return $movie;
    }

    /**
    * @param Request $request
	* @return \Illuminate\Http\JsonResponse
	* @throws \Illuminate\Validation\ValidationException
	* @createdBy anaro87
	* @createdAt 6/13/2020
	* @Description: POST
	*/
    public function store(Request $request)
    {
        
        //validate admin role
        $user = New User();
        $isAdmin = $user->isAdmin() == true ? true : $user->isAdmin();

        if($isAdmin == false){
            return response()->json([
                'success' => false,
                'message' => 'Sorry, only admin can edit fields'
            ], 403);
        }

        try {
                
            // dd($request->all());
            $isValid = $request->validate([
                'title' => 'required',
                'description' => 'required|string',
                'stock' => 'required|integer',
                'rental_price' => 'required|regex:/^\d*(\.\d{2})?$/',
                'sale_price' => 'required|regex:/^\d*(\.\d{2})?$/',
                'availability' => 'required|boolean',
                'img_url' => 'string'
            ]);
               
            $movie = new Movie();
            $movie->title = $request->title;
            $movie->description = $request->description;
            $movie->stock = $request->stock;
            $movie->rental_price = $request->rental_price;
            $movie->sale_price = $request->sale_price;
            $movie->penalty_by_day = $request->penalty_by_day;
            $movie->availability = $request->availability;
             $movie->img_url = $request->img_url;
            $movie->created_at = Carbon::now();
            $insert = $movie->save();

            if ($insert) {
                return response()->json([
                        'success' => true,
                    ]);
            } else {
                return response()->json([
                        'success' => false,
                        'message' => 'Sorry, Movie could not be added'
                    ], 500);
            }
                
        } catch (Exception $e) {
                var_dump($e);
                
        }
    }

    /**
    * @param Request $request
 	* @param $id
 	* @return \Illuminate\Http\JsonResponse
	* @createdBy anaro87
	* @createdAt 6/13/2020
	* @Description: PUT
	*/
    public function update(Request $request, $id)
    {
        //validate admin role
        $user = New User();
        $isAdmin = $user->isAdmin() == true ? true : $user->isAdmin();

        if($isAdmin == false){
            return response()->json([
                'success' => false,
                'message' => 'Sorry, only admin can edit fields'
            ], 403);
        }

        $movie = Movie::find($id);
        $movieLog = New movieLog();
    
        if (!$movie) {
            return response()->json([
                'success' => false,
                'message' => "Sorry, Movie with id {$id}  was not be found"
            ], 400);
        }

        
        if (isset($request->title) && $request->title != $movie->title) {
            $movieLog->saveLog("title", $movie->title, $request->title);
        }
        if (isset($request->rental_price) && $request->rental_price != $movie->rental_price) {
            $movieLog->saveLog("rental_price", $movie->rental_price, $request->rental_price);
        } 
        if (isset($request->sale_price) && $request->sale_price !=  $movie->sale_price) {
             $movieLog->saveLog("sale_price", $movie->sale_price, $request->sale_price);
        }

        $updated = $movie->fill($request->all())->save();
    
        if ($updated) {
            return response()->json([
                'success' => true
            ]);
        } else {
            return response()->json([
                'success' => false,
                'message' => 'Sorry, Movie could not be updated'
            ], 500);
        }
    }

    /**
	 * @param $id
	 * @return \Illuminate\Http\JsonResponse
	 * @createdBy anaro87
	 * @createdAt 6/13/2020
	 * @Description: DELETE
 	*/
    public function destroy($id)
    {
        
        //validate admin role
        $user = New User();
        $isAdmin = $user->isAdmin() == true ? true : $user->isAdmin();

        if($isAdmin == false){
            return response()->json([
                'success' => false,
                'message' => 'Sorry, only admin can delete a movie'
            ], 403);
        }

        $movie = Movie::find($id);
    
        if (!$movie) {
            return response()->json([
                'success' => false,
                'message' => "Sorry, Movie with id {$id} was not be found"
            ], 400);
        }
    
        if ($movie->delete()) {
            return response()->json([
                'success' => true
            ]);
        } else {
            return response()->json([
                'success' => false,
                'message' => 'Movie could not be deleted'
            ], 500);
        }
    }

    /**
    * @param Request $request
    * @param $id
    * @return \Illuminate\Http\JsonResponse
    * @createdBy anaro87
    * @createdAt 6/13/2020
    * @Description: PUT
    */
    public function remove(Request $request, $id)
    {
        $movie = Movie::find($id);
        $movieLog = New movieLog();
        $user = New User();
        $authUser = Auth::user();
        $userRole = $user->getUserRole($authUser->id);
    
    
        if (!$movie) {
            return response()->json([
                'success' => false,
                'message' => "Sorry, Movie with id {$id}  was not be found"
            ], 400);
        }

        if (isset($request->availability) && $request->availability !=  $movie->availability && $userRole != "admin") {
             return response()->json([
                'success' => false,
                'message' => 'Sorry, only admin can edit availability field'
            ], 403);
        }

        // dd($request->all());
        $updated = DB::table('movies')
                  ->where('id', $id)
                  ->update(['availability' => $request->availability, 'updated_at' => now()]);
    
        if ($updated) {
            return response()->json([
                'success' => true
            ]);
        } else {
            return response()->json([
                'success' => false,
                'message' => 'Sorry, Movie could not be updated'
            ], 500);
        }
    }

}
