<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Purchase extends Model
{
    protected $fillable = ['total_amount', 'qty', 'client_id', 'movie_id'];
}
